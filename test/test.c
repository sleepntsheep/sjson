#include "../sjson.h"
#include "../sjson.c"
#include <stdio.h>

void sjson_debug(struct sjson j)
{
    struct sjson_buffer *bf;
    int rc = sjson_serialize_easy(j, &bf);
    if (rc != SjsonOK)
        puts(sjson_strerror(rc));
    assert(rc == SjsonOK);
    puts(bf->a);
    sjson_buffer_cleanup(bf);
}

int main() {
    struct sjson obj;
    char *s = "{\"Test\": \"ShinLena\"}";
    int rc = sjson_parse(s, -1, NULL, &obj);
    if (rc != SjsonOK)
        fprintf(stderr, "Failed parsing: %s\n", sjson_strerror(rc));

    struct sjson str2;
    sjson_make_string("Cute", -1, &str2);
    sjson_object_set(obj, "Theo", -1, str2);

    struct sjson_buffer *serialized = NULL;
    rc = sjson_serialize_easy(obj, &serialized);
    if (rc != SjsonOK)
        fprintf(stderr, "Failed serializing: %s\n", sjson_strerror(rc));
    
    puts(serialized->a);
    sjson_buffer_cleanup(serialized);

    sjson_foreach_macro(obj, it)
    {
        struct sjson_buffer *s1 = NULL;
        (void)sjson_serialize_easy(it->v, &s1);
        puts(s1->a);
        sjson_buffer_cleanup(s1);
    }

    {
        struct sjson cute;
        assert(sjson_query_get(obj, "['Theo']", -1, &cute) == SjsonOK);
        puts(cute.v.string->a);
    }

    struct sjson arr1;
    sjson_make_array(&arr1);
    for (int i = 0; i < 10; i++) {
        struct sjson rd;
        sjson_make_number(rand() % 10, &rd);
        sjson_array_push(arr1, rd);
    }
    sjson_object_set(obj, "Rands", -1, arr1);

    {
        struct sjson rd;
        assert(sjson_query_get(obj, "['Rands'][4]", -1, &rd) == SjsonOK);
        printf("%lf", rd.v.number);
    }

    {
        struct sjson rd;
        assert(sjson_query_get(obj, "['Rands'][11]", -1, &rd) == SjsonOutOfBound);
    }
    sjson_cleanup(obj);
    return 0;

#if 0
    struct sjson j;
    sjson_parse("[1, 2, 3]", -1, NULL, &j);
    sjson_debug(&j);
    sjson_cleanup(j);

    sjson_parse("[]", -1, NULL, &j);
    sjson_debug(&j);
    sjson_cleanup(j);

    char *end;
    char *s = "[][]";
    sjson_parse(s, 2, &end, &j);
    assert(end == s + 2);
    sjson_debug(&j);
    sjson_cleanup(j);

    s = "{\"Test\": \"ShinLena\"}";
    sjson_parse(s, -1, &end, &j);
    struct sjson cute;
    sjson_make_string("Cute", -1, &cute);
    sjson_object_set(j, "Theo", -1, cute);
    assert(end == strchr(s, 0));
    assert(j.v.child);
    assert(j.v.child->len == 2);
    assert(!strcmp(j.v.child->a[0].k->a, "Test"));
    assert(j.v.child->a[0].v.type == SjsonString);
    sjson_debug(&j);
    struct sjson b = j;

    int rc = sjson_parse("[{}, [], []]", -1, NULL, &j);
    sjson_array_push(j, b);
    sjson_debug(&j);
    sjson_cleanup(j);

    char a[10005] = {0};
    FILE *fp = fopen("/home/sheep/plc.dat", "r");
    fseek(fp, 0L, SEEK_END);
    long sz = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    fread(a, sz, 1, fp);
    
    rc = sjson_parse(a, -1, NULL, &j);
    sjson_debug(&j);
    sjson_cleanup(j);

    rc = sjson_make_string("Test", -1, &j);
    sjson_debug(&j);
    sjson_cleanup(j);
#endif
}

