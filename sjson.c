#include "sjson.h"
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#define RCCHK(a) \
    do { \
        int rc = a; \
        if (rc != SjsonOK) \
            return rc; \
    } while(0)

static inline bool sjson_is_whitespace(char c)
{
    return c == ' ' || c == '\n' || c == '\t' || c == '\f';
}

static double sjson_strntod(const char *s, char **endptr, size_t n)
{
    if (n == -1)
        return strtod(s, endptr);
    if (!s)
    {
        errno = EINVAL;
        return 0;
    }
    char *tmp = malloc(n + 1);
    if (!tmp)
    {
        *endptr = (char*)s;
        errno = ENOMEM;
        return 0;
    }
    memcpy(tmp, s, n);
    tmp[n] = 0;
    double result = strtod(tmp, endptr);
    if (endptr)
        *endptr = (char*)s + (*endptr - tmp);
    free(tmp);
    return result;
}

sjson_code sjson_vector_init(struct sjson_vector **vec)
{
    struct sjson_vector *r = malloc(sizeof(*r)
                        + sizeof(r->a[0]) * SJSON_ALLOC_INIT);
    if (!r)
        return SjsonNoMem;
    r->alloc = SJSON_ALLOC_INIT;
    r->len = 0;
    *vec = r;
    return SjsonOK;
}

sjson_code sjson_vector_push(struct sjson_vector *vec, 
        struct sjson_buffer *key, struct sjson child)
{
    if (vec->len == vec->alloc)
    {
        size_t newalloc = vec->alloc * 2;
        vec = realloc(vec, sizeof(*vec) +
                sizeof(vec->a[0]) * newalloc);
        vec->alloc = newalloc;
    }
    vec->a[vec->len].k = key;
    vec->a[vec->len].v = child;
    vec->len++;
    return SjsonOK;
}

sjson_code sjson_vector_pop(struct sjson_vector *vec,
        struct sjson_buffer **kout, struct sjson *vout)
{
    if (vec->len == 0)
        return SjsonOutOfBound;
    if (kout) *kout = vec->a[vec->len--].k;
    if (vout) *vout = vec->a[vec->len--].v;
    return SjsonOK;
}

sjson_code sjson_vector_cleanup(struct sjson_vector *vec)
{
    free(vec);
    return SjsonOK;
}

sjson_code sjson_buffer_init(struct sjson_buffer **abuf)
{
    *abuf = malloc(sizeof(**abuf) + SJSON_ALLOC_INIT);
    if (!abuf)
        return SjsonNoMem;
    (*abuf)->alloc = 4;
    (*abuf)->len = 0;
    return SjsonOK;
}

sjson_code sjson_buffer_push(struct sjson_buffer **abuf,
        const void *ptr, size_t len)
{
    struct sjson_buffer *p = *abuf;
    while (p->len + len > p->alloc) {
        p = realloc(p, sizeof(*p) + (p->alloc *= 2));
        if (!p)
            return SjsonNoMem;
    }
    memcpy(p->a + p->len, ptr, len);
    p->len += len;
    *abuf = p;
    return SjsonOK;
}

void sjson_buffer_cleanup(struct sjson_buffer *buf)
{
    free(buf);
}

static sjson_code sjson_parse_array(const char *s, size_t slen,
        const char **endout, struct sjson *out)
{
    if (slen == -1)
        slen = strlen(s);
    sjson_make_array(out);
    const char *p = s + 1;
    while (p < s + slen && *p != ']')
    {
        while (p < s + slen && sjson_is_whitespace(*p))
            p++;
        struct sjson child;
        RCCHK(sjson_parse(p, slen - (p - s), (char**)&p, &child));
        RCCHK(sjson_vector_push(out->v.child, NULL, child));
        while (p < s + slen && sjson_is_whitespace(*p))
            p++;
        if (*p == ',')
            p++;
        else if (*p != ']')
            return SjsonInvalidInput;
    }
    if (p >= s + slen) return SjsonInvalidInput;
    if (endout) *endout = p + 1;
    return SjsonOK;
}

static sjson_code sjson_parse_object(const char *s, size_t slen,
        const char **endout, struct sjson *out)
{
    if (slen == -1)
        slen = strlen(s);
    out->type = SjsonObject;
    sjson_vector_init(&out->v.child);

    const char *p = s + 1;
    while (p < s + slen && *p != '}')
    {
        while (p < s + slen && sjson_is_whitespace(*p))
            p++;
        struct sjson key_string;
        RCCHK(sjson_parse(p, slen - (p - s), (char**)&p, &key_string));
        while (p < s + slen && sjson_is_whitespace(*p))
            p++;
        if (*p++ != ':')
            return SjsonInvalidInput;
        struct sjson value;
        RCCHK(sjson_parse(p, slen - (p - s), (char**)&p, &value));
        sjson_vector_push(out->v.child, key_string.v.string, value);
        while (p < s + slen && sjson_is_whitespace(*p))
            p++;
        if (p < s + slen && *p == ',')
            p++;
        else if (p < s + slen && *p != '}')
            return SjsonInvalidInput;
    }
    if (p >= s + slen) return SjsonInvalidInput;
    if (endout) *endout = p + 1;
    return SjsonOK;
}

static sjson_code sjson_parse_escape(const char *s, size_t slen,
        struct sjson_buffer **out)
{
    if (slen == -1)
        slen = strlen(s);

    RCCHK(sjson_buffer_init(out));

    const char *p = s;
    while (p < s + slen)
    {
        if (*p == '\\' && p + 1 < s + slen)
        {
            switch (p[1])
            {
                case '\\':
                    sjson_buffer_push(out, "\\", 1);
                    break;
                case 'n':
                    sjson_buffer_push(out, "\n", 1);
                    break;
                case 'f':
                    sjson_buffer_push(out, "\n", 1);
                    break;
                case 'r':
                    sjson_buffer_push(out, "\r", 1);
                    break;
                case 't':
                    sjson_buffer_push(out, "\t", 1);
                    break;
                case '\"':
                    sjson_buffer_push(out, "\"", 1);
                    break;
                case '/':
                    sjson_buffer_push(out, "/", 1);
                    break;
                default:
                    break;
            }
            p++;
        }
        else
        {
            sjson_buffer_push(out, p, 1);
            p++;
        }
    }
    sjson_buffer_push(out, "\x0", 1);
    return SjsonOK;
}

static sjson_code sjson_parse_string(const char *s, size_t slen,
        const char **endptr, struct sjson *out)
{
    if (slen == -1)
        slen = strlen(s);
    const char *e = s + 1;
    while (e < s + slen && !(*e == '\"' && *e != '\\'))
        e++;
    if (*e != '\"')
        return SjsonInvalidInput;
    out->type = SjsonString;
    struct sjson_buffer *key;
    RCCHK(sjson_parse_escape(s + 1, e - (s + 1), &key));
    out->v.string = key;
    *endptr = e + 1;
    return SjsonOK;
}

sjson_code sjson_cleanup(struct sjson json)
{
    switch (json.type)
    {
        case SjsonString:
            sjson_buffer_cleanup(json.v.string);
            break;
        case SjsonArray:
        case SjsonObject:
            sjson_foreach_macro(json, it)
            {
                if (it->k) sjson_buffer_cleanup(it->k);
                sjson_cleanup(it->v);
            }
            sjson_vector_cleanup(json.v.child);
            break;
        default:
            break;
    }
    return SjsonOK;
}

sjson_code sjson_parse(const char *s, size_t slen,
        char **endptr, struct sjson *out)
{
    if (slen == -1)
        slen = strlen(s);

    const char *p = s;
    struct sjson j = { 0 };
    while (p < s + slen && sjson_is_whitespace(*p)) p++;
    switch (p[0])
    {
        case 'n':
            if (!strncmp(p, "null", 4)) {
                j.type = SjsonNull;
                p += 4;
            } else {
                return SjsonInvalidInput;
            }
            break;
        case 't':
            if (!strncmp(p, "true", 4)) {
                j.type = SjsonTrue;
                p += 4;
            } else {
                return SjsonInvalidInput;
            }
            break;
        case 'f':
            if (!strncmp(p, "false", 5)) {
                j.type = SjsonFalse;
                p += 5;
            } else {
                return SjsonInvalidInput;
            }
            break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
        case '-': case '+': 
        {
            const char *end = p;
            while ((*end <= '9' && *end >= '0') || *end == '.')
                end++;
            char *end2;
            j.type = SjsonNumber;
            j.v.number = sjson_strntod(p, &end2, end - p);
            // TODO - remove assert
            assert(end2 == end);
            p = end;
            break;
        }
        case '\"':
            RCCHK(sjson_parse_string(p, slen - (p - s), &p, &j));
            break;
        case '[':
            RCCHK(sjson_parse_array(p, slen - (p - s), &p, &j));
            break;
        case ']':
            return SjsonInvalidInput;
        case '{':
            RCCHK(sjson_parse_object(p, slen - (p - s), &p, &j));
            break;
        case '}':
            return SjsonInvalidInput;
        case ' ':
        case '\n':
        case '\t':
        case '\f':
            break;
        default:
            break;
    }
    *out = j;
    if (endptr) *endptr = (char*)p;
    return SjsonOK;
}

const char *sjson_strerror(sjson_code errcode) {
    if (errcode < 0 || errcode >= SjsonCodeCount)
        return "Unknown error code.";
    static const char *errors[] = {
        [SjsonOK] = "No error.",
        [SjsonInvalidInput] = "Invalid input.",
        [SjsonNoMem] = "Failed to allocate memory.",
        [SjsonNotFound] = "Requested member not found.",
        [SjsonOutOfBound] = "Tried to access array element out of bound",
        [SjsonNullDeref] = "Tried to dereference NULL pointer",
        [SjsonWrongType] = "Wrong type of json passed.",
    };
    return errors[errcode];
}

sjson_code sjson_object_get(struct sjson object, const char *key,
        size_t keylen, struct sjson *out)
{
    if (object.type != SjsonObject)
        return SjsonWrongType;
    if (keylen == -1)
        keylen = strlen(key);
    sjson_foreach_macro(object, it)
    {
        if (it->k->len - 1 != keylen)
            continue;
        if (strncmp(it->k->a, key, keylen) != 0)
            continue;
        *out = it->v;
        return SjsonOK;
    }
    return SjsonNotFound;
}

sjson_code sjson_object_set(struct sjson object, const char *key,
        size_t keylen, struct sjson elem)
{
    if (object.type != SjsonObject)
        return SjsonWrongType;
    if (keylen == -1)
        keylen = strlen(key);
    sjson_foreach_macro(object, it)
    {
        if (it->k->len != keylen)
            continue;
        if (strncmp(it->k->a, key, keylen) != 0)
            continue;
        it->v = elem;
        return SjsonOK;
    }
    struct sjson_buffer *nb;
    RCCHK(sjson_buffer_init(&nb));
    RCCHK(sjson_buffer_push(&nb, key, keylen));
    RCCHK(sjson_buffer_push(&nb, "\0", 1));
    RCCHK(sjson_vector_push(object.v.child, nb, elem));
    return SjsonNotFound;
}

sjson_code sjson_array_get(struct sjson object, size_t idx, struct sjson *out)
{
    if (object.type != SjsonArray)
        return SjsonWrongType;
    size_t i = 0;
    sjson_foreach_macro(object, it)
    {
        if (i == idx)
        {
            *out = it->v;
            return SjsonOK;
        }
        i++;
    }
    return SjsonOutOfBound;
}

sjson_code sjson_array_set(struct sjson array, size_t idx, struct sjson elem)
{
    if (array.type != SjsonArray)
        return SjsonWrongType;
    size_t i = 0;
    sjson_foreach_macro(array, it)
    {
        if (i == idx)
        {
            it->v = elem;
            return SjsonOK;
        }
        i++;
    }
    return SjsonOutOfBound;
}

sjson_code sjson_array_push(struct sjson array, struct sjson elem)
{
    RCCHK(sjson_vector_push(array.v.child, NULL, elem));
    return SjsonOK;
}

static sjson_code sjson_serialize_easy_helper(struct sjson j, struct sjson_buffer **buf)
{
    char b[128];
    switch (j.type)
    {
        case SjsonTrue:
            sjson_buffer_push(buf, "true", 4);
            break;
        case SjsonFalse:
            sjson_buffer_push(buf, "false", 5);
            break;
        case SjsonNull:
            sjson_buffer_push(buf, "null", 4);
            break;
        case SjsonString:
            sjson_buffer_push(buf, "\"", 1);
            sjson_buffer_push(buf, j.v.string->a, j.v.string->len - 1);
            sjson_buffer_push(buf, "\"", 1);
            break;
        case SjsonNumber:
            snprintf(b, sizeof b, "%lf", j.v.number);
            sjson_buffer_push(buf, b, strlen(b));
            break;
        case SjsonArray:
            {
                size_t i = 0;
                sjson_buffer_push(buf, "[", 1);
                sjson_foreach_macro(j, it)
                {
                    int rc = sjson_serialize_easy_helper(it->v, buf);
                    if (rc != SjsonOK)
                        return rc;
                    if (i != j.v.child->len - 1)
                        sjson_buffer_push(buf, ",", 1);
                    i++;
                }
                sjson_buffer_push(buf, "]", 1);
                break;
            }
        case SjsonObject:
            {
                size_t i = 0;
                sjson_buffer_push(buf, "{", 1);
                sjson_foreach_macro(j, it)
                {
                    sjson_buffer_push(buf, "\"", 1);
                    sjson_buffer_push(buf, it->k->a, it->k->len - 1);
                    sjson_buffer_push(buf, "\":", 2);
                    int rc = sjson_serialize_easy_helper(it->v, buf);
                    if (rc != SjsonOK)
                        return rc;
                    if (i != j.v.child->len - 1)
                        sjson_buffer_push(buf, ",", 1);
                    i++;
                }
                sjson_buffer_push(buf, "}", 1);
                break;
            }
        case SjsonInvalid:
            break;
    }
    return SjsonOK;
}

sjson_code sjson_serialize_easy(struct sjson j, struct sjson_buffer **buf)
{
    sjson_code rc;
    rc = sjson_buffer_init(buf);
    if (rc != SjsonOK) return rc;
    rc = sjson_serialize_easy_helper(j, buf);
    if (rc != SjsonOK) return rc;
    rc = sjson_buffer_push(buf, "\0", 1);
    if (rc != SjsonOK) return rc;
    return SjsonOK;
}


/* utility function */
sjson_code sjson_make_string(const char *s, size_t slen, struct sjson *out)
{
    if (slen == -1)
        slen = strlen(s);
    out->type = SjsonString;
    RCCHK(sjson_buffer_init(&out->v.string));
    RCCHK(sjson_buffer_push(&out->v.string, s, slen));
    RCCHK(sjson_buffer_push(&out->v.string, "\0", 1));
    return SjsonOK;
}

void sjson_make_number(double number, struct sjson *out)
{
    out->type = SjsonNumber;
    out->v.number = number;
}

void sjson_make_true(struct sjson *out)
{
    out->type = SjsonTrue;
}

void sjson_make_false(struct sjson *out)
{
    out->type = SjsonFalse;
}

void sjson_make_null(struct sjson *out)
{
    out->type = SjsonNull;
}

sjson_code sjson_make_array(struct sjson *out)
{
    out->type = SjsonArray;
    sjson_vector_init(&out->v.child);
    return SjsonOK;
}

sjson_code sjson_make_object(struct sjson *out)
{
    out->type = SjsonObject;
    sjson_vector_init(&out->v.child);
    return SjsonOK;
}

sjson_code sjson_query_get(struct sjson json, const char *q,
        size_t qlen, struct sjson *out)
{
    if (qlen == -1)
        qlen = strlen(q);

    const char *p = q;
    struct sjson cur = json;
    while (p < q + qlen)
    {
        if (*p++ != '[')
            return SjsonInvalidInput;
        if (*p == '\'')
        {
            const char *e = p + 1;
            while (e < q + qlen && (*e != '\'' || *(e-1) == '\\'))
                e++;
            if (e == q + qlen)
                return SjsonInvalidInput;
            RCCHK(sjson_object_get(cur, p + 1, e - p - 1, &cur));
            p = e + 2;
        }
        else if (p[1] >= 0 || p[1] <= 9)
        {
            char *e = strchr(p, ']');
            if (!e)
                return SjsonInvalidInput;
            size_t index = 0;
            for (const char *pp = p; pp < e; pp++)
            {
                if (*pp <= '0' || *pp >= '9')
                    return SjsonInvalidInput;
                index *= 10;
                index += *pp - '0';
            }
            RCCHK(sjson_array_get(cur, index, &cur));
            p = e + 1;
        }
    }
    *out = cur;
    return SjsonOK;
}

#if 0
sjson_code sjson_query_set(struct sjson json, const char *q, size_t qlen,
        struct sjson elem)
{
    if (qlen == -1)
        qlen = strlen(q);

    const char *p = q;
    struct sjson cur = json;
    while (p < q + qlen)
    {
        if (*p++ != '[')
            return SjsonInvalidInput;
        if (*p == '\'')
        {
            char *e = p + 1;
            while (e < q + qlen && (*e != '\'' || *(e-1) == '\\'))
                e++;
            if (e == q + qlen)
                return SjsonInvalidInput;
            RCCHK(sjson_object_get(cur, p + 1, e - p - 1, &cur));
            p = e + 2;
        }
        else if (p[1] >= 0 || p[1] <= 9)
        {
            char *e = strchr(p, ']');
            if (!e)
                return SjsonInvalidInput;
            size_t index = 0;
            for (char *pp = p; pp < e; pp++)
            {
                if (*pp <= '0' || *pp >= '9')
                    return SjsonInvalidInput;
                index *= 10;
                index += *pp - '0';
            }
            RCCHK(sjson_array_get(cur, index, &cur));
            p = e + 1;
        }
    }
    *out = cur;
    return SjsonOK;
}
#endif

