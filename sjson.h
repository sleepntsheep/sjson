/**
 * @file sjson.h
 * @brief simple json parser
 *
 * Author - sleepntsheep 2022
 *
 */
#pragma once

#ifndef SJSON_H
#define SJSON_H
#include <stdbool.h>
#include <stddef.h>
#define SJSON_ALLOC_INIT 16

typedef enum {
    SjsonInvalid,
    SjsonNull,
    SjsonTrue,
    SjsonFalse,
    SjsonString,
    SjsonNumber,
    SjsonArray,
    SjsonObject,
} sjson_type;

typedef enum {
    SjsonOK,
    SjsonInvalidInput,
    SjsonNoMem,
    SjsonNotFound,
    SjsonOutOfBound,
    SjsonChildNotInitialized,
    SjsonWrongType,
    SjsonNullDeref,
    SjsonCodeCount,
} sjson_code;

typedef int sjson_result;

struct sjson_array;

struct sjson {
    sjson_type type;
    union {
        double number;
        struct sjson_buffer *string;
        struct sjson_vector *child;
    } v;
};

 /**
  * @brief resizable buffer, used for string datatype and object key,
  * meant to be used as a pointer.
  * Beware - len member store NUL terminator too, so you might
  * wanna account for that if you will use it.
  */
struct sjson_buffer {
    size_t alloc; /** amount of memory allocated for buffer */
    size_t len; /** length of data in buffer */
    char a[]; /** buffer */
};

/**
 * @brief pair of sjson_buffer and sjson, used in object json type.
 */
struct sjson_keyvalue {
    struct sjson_buffer *k; /** key */
    struct sjson v; /** value */
};

/**
 * @brief dynamic array of sjson_keyvalue, used for json object and array
 * json datatype.
 */
struct sjson_vector {
    size_t alloc;
    size_t len;
    struct sjson_keyvalue a[];
};

/**
 * @brief Return corresponding error string from error code
 * */
const char *sjson_strerror(sjson_code errcode);

sjson_code sjson_parse(const char *s, size_t slen,
        char **endptr, struct sjson *out);
sjson_code sjson_serialize_easy(struct sjson json, struct sjson_buffer **out);
sjson_code sjson_cleanup(struct sjson json);

sjson_code sjson_object_get(struct sjson object, const char *key,
        size_t keylen, struct sjson *out);
sjson_code sjson_object_delete(struct sjson object, const char *key,
        size_t keylen);

sjson_code sjson_array_get(struct sjson array, size_t idx, struct sjson *out);
sjson_code sjson_array_delete(struct sjson array, size_t idx, struct sjson *out);
sjson_code sjson_array_set(struct sjson array, size_t idx, struct sjson elem);
sjson_code sjson_array_push(struct sjson array, struct sjson elem);

sjson_code sjson_object_set(struct sjson json, const char *key, size_t keylen, struct sjson elem);
sjson_code sjson_object_get(struct sjson json, const char *key, size_t keylen, struct sjson *out);

#define sjson_foreach_macro(json, iterator) \
    for (struct sjson_keyvalue *iterator = json.v.child->a; \
        iterator - json.v.child->a < json.v.child->len; \
        iterator++)

// TODO : simple query language like
// ['key_one'][2]['name'] to traverse nested json, set out to NULL if not found
sjson_code sjson_query_get(struct sjson json, const char *query, size_t querylen,
        struct sjson *out);
// similar to get, but create the path; if doesn't exists
// UNIMPLEMENTED
#if 0
sjson_code sjson_query_set(struct sjson json, const char *query, size_t querylen,
        struct sjson elem);
#endif

/* vector */
sjson_code sjson_vector_init(struct sjson_vector **vec);
sjson_code sjson_vector_push(struct sjson_vector *vec, 
        struct sjson_buffer *key, struct sjson child);
sjson_code sjson_vector_pop(struct sjson_vector *vec, 
        struct sjson_buffer **kout, struct sjson *vout);
sjson_code sjson_vector_cleanup(struct sjson_vector *vec);

/* buffer */
sjson_code sjson_buffer_init(struct sjson_buffer **buf);
sjson_code sjson_buffer_push(struct sjson_buffer **buf, const void *ptr, size_t n);
void sjson_buffer_cleanup(struct sjson_buffer *buf);

/* utility function */
sjson_code sjson_make_string(const char *s, size_t slen, struct sjson *out);
void sjson_make_number(double number, struct sjson *out);
void sjson_make_true(struct sjson *out);
void sjson_make_false(struct sjson *out);
void sjson_make_null(struct sjson *out);
sjson_code sjson_make_array(struct sjson *out);
sjson_code sjson_make_object(struct sjson *out);

#endif /* SJSON_H */

