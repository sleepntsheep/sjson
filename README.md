# sjson

Simple json parser.

### Usage

On every function that take in a string and a length of that string,
you can pass `-1` as length if the string is NUL terminated.
And on every function that take 2-level pointer `endptr`,
you can pass NULL if you don't want to know where that json end.
    
    struct sjson obj;
    char *s = "{\"Test\": \"ShinLena\"}";
    sjson_code rc = sjson_parse(s, -1, &end, &obj);
    if (rc != SjsonOK)
        fprintf(stderr, "Failed parsing: %s\n", sjson_strerror(rc));

    struct sjson str2;
    sjson_make_string("Cute", -1, &str2);
    sjson_object_set(obj, "Theo", -1, str2);

    struct sjson_buffer *serialized;
    rc = sjson_serialize_easy(*j, &serialized);
    if (rc != SjsonOK)
        fprintf(stderr, "Failed serializing: %d\n", sjson_strerror(rc));
    
    puts(serialized->a);
    sjson_buffer_cleanup(serialized);

You can loop over an array or object by `sjson_foreach_macro(parent, iterator)`.
At `i-th` iteration, `iterator` would point to `i-th` `sjson_keyvalue`.
Then you can access key (if parent is object) by `it->k` or json value by `it->v`.
If parent is NULL, `it->k` would be NULL.

    sjson_foreach_macro(obj, it)
    {
        struct sjson_buffer *s1 = NULL;
        (void)sjson_serialize_easy(it->v, &s1);
        puts(s1->a);
        sjson_buffer_cleanup(s1);
    }

You can also query a nested json structure easily.
For example if I have this
    
    {
        "numbers": [
            1, 29, 329402934, 09193, -12398
        ],
        "86": {
            "Shin": "Lena"
        }
    }

And I parsed it into a `struct json` called object, I can get the "Lena" json like this.

    struct sjson lena;
    sjson_query_get(object, "['86']['Shin']", &lena);

You can use it on arrays too.

    struct sjson num29;
    sjson_query_get(object, "['numbers'][1]", &num29);


### Data structure.

    struct sjson {
        sjson_type type;
        union {
            double number;
            struct sjson_buffer *string;
            struct sjson_vector *child;
        } v;
    };

struct sjson represent a json data, this struct is not meant to be
used as pointer, except when passing it as output argument on function
like `sjson_parse`.

child of array and object type are stored as a **flat map**,
so accessing an object by key is `O(n)`.

### TODO

 - [ ] `\uDEAD` unicode escape parser.
 - [ ] escape sequence on serializer.


